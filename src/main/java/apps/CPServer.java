package apps;

import core.Msg;
import cp.CPProtocol;
import exceptions.IWProtocolException;
import phy.PhyProtocol;

import java.io.IOException;

public class CPServer {
    protected static final int SERVER_PORT = 3027;

    public static void printMessage(String cookie, int messageId, String message) {
        //printing recieved message on screen
        System.out.println("Received message: " + message + " id: " + messageId + " with cookie: " + cookie);
    }
    public static void main(String[] args) {
        // Set up the virtual link protocol
        PhyProtocol phy = new PhyProtocol(SERVER_PORT);

        // Set up command protocol
        CPProtocol cp = null;
        try {
            cp = new CPProtocol(phy);
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Start server processing
        boolean eof = false;
        while (!eof) {
            try {
                Msg msg = cp.receive();
                String sentence = msg.getData().trim();
                System.out.println("Received message: " + sentence);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}