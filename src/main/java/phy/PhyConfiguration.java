package phy;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

import core.Configuration;
import core.Protocol;

public class PhyConfiguration extends Configuration{
	protected int remotePort;
	protected InetAddress remoteIPAddress;
	protected Protocol.proto_id pid;
	protected boolean isClient;
	
	public PhyConfiguration(InetAddress rip, int rp, Protocol.proto_id pid) throws UnknownHostException {
		super(null);
		this.remotePort = rp;
		this.remoteIPAddress = rip;
		this.pid = pid;
		this.isClient = true;
	}

	public int getRemotePort() {
		return this.remotePort;
	}
	
	public InetAddress getRemoteIPAddress () {
		return this.remoteIPAddress;
	}
	public Protocol.proto_id getPid() {return this.pid;}


	@Override
	public boolean equals(Object o) {

		if(o instanceof PhyConfiguration phy1){ //no "null check" needed, because instanceof already covers that

			return phy1.hashCode() == this.hashCode(); //if the hash code is the same, the objects are the same
		}

		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(remotePort, remoteIPAddress, pid, isClient);
	}
}
