package cp;

import core.Msg;
import exceptions.IllegalMsgException;

class CPCookieResponseMsg extends CPMsg {
    protected static final String CP_CRES_HEADER = "cres";
    private boolean success;

    protected CPCookieResponseMsg() {

    }
    protected CPCookieResponseMsg(boolean s) {
        this.success = s;
    }

    protected boolean getSuccess() {return this.success;}

    /*
     * Create cookie request message.
     * The cp header is prepended in the super-class.
     */
    @Override
    protected void create(String data) {
        if (this.success) {
            // prepend cres header
            data = CP_CRES_HEADER + " ACK " + data;
        } else {
            data = CP_CRES_HEADER + " NAK " + data;
        }
        // super class prepends slp header
        super.create(data);
    }

    protected Msg parse(String sentence) throws IllegalMsgException {
        if (!sentence.startsWith(CP_CRES_HEADER)) {
            throw new IllegalMsgException();
        }
        String[] parts = sentence.split("\\s+", 3);

        if(parts[1].equals("ACK"))
            this.success = true;
        else
            this.success = false;

        this.data = parts[2];
        return this;
    }

}
