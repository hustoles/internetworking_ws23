package cp;

import core.Msg;
import exceptions.IWProtocolException;
import exceptions.IllegalMsgException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.CRC32;

public class CPCommandResponseMessage extends CPMsg{

    private int currentMessageId;
    protected static final String COMRES_HEADER = "comres";

    CPCommandResponseMessage(){}

    public int getMessageId(){
        return currentMessageId;
    }




    @Override
    protected Msg parse(String sentence) throws IWProtocolException {

        //dismantelts the received frame into its components
        String[] fields = sentence.split(" ");
        //the "actual" Message might have whitespaces in it self. so we need to put it back together later with the help of the messageLength field
        StringBuilder reconstructedMessage = new StringBuilder();
        int messageLength;

        //first check, to see if the received frame is valid.
        if(!fields[0].equals("cp") || !fields[1].equals(COMRES_HEADER)){
            throw new IllegalMsgException();
        }

        try {
            this.currentMessageId = Integer.parseInt(fields[2]);
            //throws NumberFormatException, if the id couldnt get parsed for whatever reason

            //throws exception if the id from the received message is too large or negative, wich it cant be according to protocol specification
            if(currentMessageId > CPProtocol.getMaxId() || currentMessageId < 0){
                throw new IllegalMsgException();
            }
        }catch (NumberFormatException e){
            throw new IllegalMsgException();
        }

        //sucess ok or error
        if(!fields[3].equals("ok") && !fields[3].equals("error")){
            throw new IllegalMsgException();
        }

        //get the length of the actual message. Again, might throw an exception if the length value is not a number for example
        try {
            messageLength = Integer.parseInt(fields[4]);
        }catch (NumberFormatException e){
            throw new IllegalMsgException();
        }


        //reconstructes message
        int sumLen = 0;

        //calculating the actual length of the message
        for (int i = 5; i < fields.length - 1; i++) {

            sumLen += fields[i].length(); //count sum of each real message field
            sumLen++; //count whitespace between
        }
        sumLen--; //dont count the whitespace from the last loop iteration

        //if the legth field is different from the actual message length
        if (sumLen > 0) {
            if(sumLen != messageLength){
                throw new IllegalMsgException();
            }
        }


        // Compare the CRC checksum
        String receivedChecksumStr = fields[fields.length - 1];
        long receivedChecksum;
        try {
            receivedChecksum = Long.parseLong(receivedChecksumStr, 16);
        } catch (NumberFormatException e) {
            throw new IllegalMsgException();
        }

        long calculatedChecksum = this.calcCRC(reconstructedMessage.toString());

        if (calculatedChecksum != receivedChecksum) {
            throw new IllegalMsgException();
        }


        //sets the received message as the data, so the client can print it on the console
        this.data = reconstructedMessage.toString();



        return this;
    }
}
