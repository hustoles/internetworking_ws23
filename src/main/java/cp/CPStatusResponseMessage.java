package cp;

public class CPStatusResponseMessage extends CPMsg {
    private int processedCommands;
    private long sessionTTL;

    public CPStatusResponseMessage() {
    }

    public void create(String cookie, int messageId, int processedCommands, long sessionTTL) {
        this.processedCommands = processedCommands;
        this.sessionTTL = sessionTTL;

        StringBuilder str = new StringBuilder();
        str.append("status").append(' ');
        str.append(messageId).append(' ');
        str.append(cookie).append(' ');
        str.append(processedCommands).append(' ');
        str.append(sessionTTL).append(' ');

        super.create(str.toString());

        this.data = str.toString();
        str.append(calcCRC(this.data));
        this.data = str.toString();
    }

    public int getProcessedCommands() {
        return processedCommands;
    }

    public long getSessionTTL() {
        return sessionTTL;
    }
}