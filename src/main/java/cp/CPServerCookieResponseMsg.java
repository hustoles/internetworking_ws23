package cp;

import exceptions.IWProtocolException;
import phy.PhyConfiguration;
import phy.PhyProtocol;

import java.io.IOException;

public class CPServerCookieResponseMsg extends CPCookieResponseMsg{

    CPServerCookieResponseMsg(boolean success, String responseMessage, CPCookie cookie){
        super(success);

        if(success){
            super.create(String.valueOf(cookie.getCookieValue()));
        }else {
            super.create(responseMessage);
        }
    }

    public void sendResponse(PhyProtocol phy, PhyConfiguration config) throws IWProtocolException, IOException {
        phy.send(super.data, config);
    }
}
