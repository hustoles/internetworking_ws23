package cp;

public class CPCookie {

    //error Messages for Cookie Response Message
    public static final String cookieErrorLimit = "COOKIE_ERR_LIMIT_REACHED";
    public static final String cookieErrorOldCookie = "COOKIE_ERR_OLD_COOKIE_VALID";
    public static final String cookieSuccess = "COOKIE_ERR_LIMIT_REACHED";

    private int cookieValue; //"random" Number
    private long createdAt; //time of Creation as "Unix time"
    private static final long MAX_LIFETIME_SECONDS = 600; //600 Seconds, according to protocol specification
    CPCookie(){
        this.createdAt = System.currentTimeMillis();
        this.cookieValue = 1;
    }

    public int getCookieValue() {
        return cookieValue;
    }

    public boolean isStillValid(){
        return System.currentTimeMillis() - createdAt <= 600; //returns true, if the cookie has lived 600 Seconds or less
    }
}
