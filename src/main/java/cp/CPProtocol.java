package cp;

import apps.CPServer;
import core.*;
import exceptions.*;
import phy.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Random;

public class CPProtocol extends Protocol {
    private static final int CP_TIMEOUT = 2000;
    private static final int CP_HASHMAP_SIZE = 20;
    private String cookie;
    private int id;
    private int messageIdCount = 0; //tracks the IDS
    private static final int MAX_ID = 65535; //maximum message id according to the protocol specification.
    private PhyConfiguration PhyConfig;
    private PhyProtocol PhyProto;
    boolean isClient;
    HashMap<Integer, CPCookie> cookieMap;
    Random rnd;

    // Constructor for clients
    public CPProtocol(InetAddress rname, int rp, PhyProtocol phyP) throws UnknownHostException {
        this.PhyConfig = new PhyConfiguration(rname, rp, proto_id.CP);
        this.PhyProto = phyP;
        this.isClient = true;
    }
    // Constructor for servers
    public CPProtocol(PhyProtocol phyP) {
        this.PhyProto = phyP;
        this.isClient = false;
        this.cookieMap = new HashMap<>();
        this.rnd = new Random();
    }
    @Override
    public void send(String s, Configuration config) throws IOException, IWProtocolException {
        this.send(s, "", config);
    }

    public void send(String s, String message, Configuration config) throws IOException, IWProtocolException {

        if (cookie == null) {
            // Request a new cookie from server
            // Either updates the cookie attribute or returns with an exception
            requestCookie(0);
        }

        //1.2: 1 a
        if(cookie != null) {

            //does not send message if the maximal id is exceeded. increments id when command message is allowed to be created
            if(messageIdCount >= MAX_ID){
                System.out.println("Max message Id Cont exceeded.");
                return;
            }else{
                messageIdCount++;
            }

            //1.2: 1 b
            CPCommandMessage cmdMsg = new CPCommandMessage();
            cmdMsg.create(cookie, s, message, messageIdCount);

            //remembers the id of the current message, so when it's time to receive wel can check if the id's are the same
            this.id = cmdMsg.getMessageId();

            //1.2: 1 c
            PhyProto.send(cmdMsg.getData(), this.PhyConfig);
        }
    }

    @Override
    public Msg receive() throws IOException {

        //1.2: 2 a
        Msg in = new CPMsg();
        boolean receving = true;

        while(receving){

            //1.2: 2 b
            CPCommandMessage cpmsg = new CPCommandMessage();

            try {
                in = this.PhyProto.receive(CP_TIMEOUT);

                //1.2: 2 b
                in = cpmsg.parse(in.getData());

                new CommandMessageProcessor().processCommandMessage(cpmsg);

            } catch (Exception e) {
                System.out.println(e.getMessage());
                continue;
                //Discard Message
            }

            //discard message, if not instance of CPCommandMessage
            if(!(in instanceof CPCommandResponseMessage)){
                continue;
            }

            //compare the id from the received message and the earlier sent message
            if(((CPCommandResponseMessage) in).getMessageId() != this.id){
                continue;
            }

            receving = false;
        }

        return in;
    }

    private class CommandMessageProcessor {
        public void processCommandMessage(CPCommandMessage cmdMsg) throws Exception {
            String command = cmdMsg.getCommand();
            String cookie = cmdMsg.getCookie();
            int messageId = cmdMsg.getMessageId();
            String message = cmdMsg.getMessage();
            long TTL = cmdMsg.getTTL();

            switch (command) {
                case "status":
                    processStatusCommand(cookie, messageId, Integer.parseInt(command), TTL);
                    break;
                case "print":
                    processPrintCommand(cookie, messageId, message);
                    break;
                default:
                    System.out.println("Unknown command: " + command);
                    break;
            }
        }

        private void processStatusCommand(String cookie, int messageId, int processedCommands, long sessionTTL) throws Exception {
            CPStatusResponseMessage statusResponse = new CPStatusResponseMessage();
            statusResponse.create(cookie, messageId, processedCommands, sessionTTL);

            PhyProto.send(statusResponse.getData(), CPProtocol.this.PhyConfig);
        }

        private void processPrintCommand(String cookie, int messageId, String message) {
            //print on server console
            CPServer.printMessage(cookie, messageId, message);
        }
    }


    public void cookieProcesing(PhyConfiguration config, CPCookie cookie) throws IWProtocolException, IOException {


        if(cookieMap.size() >= CP_HASHMAP_SIZE){
            new CPServerCookieResponseMsg(false,"Cookie limit reached", cookie).sendResponse(this.PhyProto, config);
            return;
        }

        CPCookie recievedCookie = cookieMap.get(config.hashCode()); //is null when there is no cookie currently saved

        if(recievedCookie != null){ //if the cookie still exists

            if(recievedCookie.isStillValid()){

                //tells the client that an old cookie has not expired
                new CPServerCookieResponseMsg(false,"Old Cookie is still valid", cookie).sendResponse(this.PhyProto, config);
            }else{
                cookieMap.remove(config.hashCode()); //removes the Cookie, because it has expired
            }
            return;
        }

        cookieMap.put(config.hashCode(), cookie);
        new CPServerCookieResponseMsg(true,"Cookie is valid", cookie).sendResponse(this.PhyProto, config);
    }


     public void requestCookie(int count) throws IOException, IWProtocolException {
        if(count >= 3)
            throw new CookieRequestException();
        CPCookieRequestMsg reqMsg = new CPCookieRequestMsg();
        reqMsg.create(null);

        this.PhyProto   .send(new String (reqMsg.getDataBytes()), this.PhyConfig);
        Msg resMsg = new CPMsg();

        try {
            Msg in = this.PhyProto.receive(CP_TIMEOUT);
            if (((PhyConfiguration)in.getConfiguration()).getPid() != proto_id.CP)
                throw new IllegalMsgException();
            resMsg = ((CPMsg) resMsg).parse(in.getData());
        } catch (SocketTimeoutException e) {
            requestCookie(count+1);
        }

        if(resMsg instanceof CPCookieResponseMsg && !((CPCookieResponseMsg) resMsg).getSuccess()) {
            throw new CookieRequestException();
        }
        this.cookie = resMsg.getData();
    }

    public static int getMaxId(){
        return MAX_ID;
    }

    public PhyProtocol getPhyProtocol(){
        return this.PhyProto;
    }
}
