package cp;

import exceptions.IllegalCommandException;

import java.util.zip.CRC32;
import java.util.zip.Checksum;

public class CPCommandMessage extends CPMsg{
    private int currentMessageId;
    private String command;
    private String cookie;
    private String message;
    protected static final String HEADER = "command";

    public CPCommandMessage() {

    }
    public boolean isCommandMessage() {
        // Check if the message is a command message based on the specified format
        // You may need to adjust this based on the actual structure of your message
        return (this.data != null && this.data.startsWith("cp") && this.data.contains("command"));
    }


    public CPCommandMessage create(String cookie, String command, String message, int messageIdCount){

        this.currentMessageId = messageIdCount;

        StringBuilder str = new StringBuilder();
        str.append(HEADER).append(' ');
        str.append(currentMessageId).append(' ');
        str.append(cookie).append(' ');
        str.append(command).append(' ');
        if(message != null && !message.isEmpty())str.append(message).append(' '); //message is optional according to protocol specification

        //calls the create method of CPMsg so the header gets added
        super.create(str.toString());

        this.data = str.toString(); //builds the Frame
        str.append(calcCRC(this.data)); //calculates the crc and adds it to the Frame
        this.data = str.toString(); //builds the Frame again, but with the attached checksum

        this.command = command;
        this.cookie = cookie;
        this.message = message;

        return this;
    }

    public String getCommand() {
        return command;
    }

    public String getCookie() {
        return cookie;
    }

    public String getMessage() {
        return message;
    }


    //returns the unique id of the message
    public int getMessageId(){
        return currentMessageId;
    }

    public long getTTL() {
        return 0;
    }
}
