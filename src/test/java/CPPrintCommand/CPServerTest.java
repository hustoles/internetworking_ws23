package CPPrintCommand;
import apps.CPServer;
import cp.CPCommandMessage;
import cp.CPProtocol;
import exceptions.IWProtocolException;
import org.junit.jupiter.api.Test;
import phy.PhyConfiguration;
import phy.PhyProtocol;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class CPServerTest {

    @Test
    void testProcessWellFormedPrintCommand() throws IOException, IWProtocolException {

        PhyProtocol mockPhyProtocol = mock(PhyProtocol.class);
        PhyConfiguration mockPhyConfig = mock(PhyConfiguration.class);
        CPProtocol cpProtocol = mock(CPProtocol.class);

        // Set up print command message
        CPCommandMessage printCommand = new CPCommandMessage();
        printCommand.create("validCookie", "print", "Hello, Server!", 1);

        // Mock the behavior getPhyProtocol() to return the mockPhyProtocol
        when(cpProtocol.getPhyProtocol()).thenReturn(mockPhyProtocol);

        CPServer.printMessage("validCookie", 1, "Hello, Server!");
        verify(mockPhyProtocol).send(eq(printCommand.getData()), eq(mockPhyConfig));
    }

    @Test
    void testProcessMalformedPrintCommand() throws IOException, IWProtocolException {

        PhyProtocol mockPhyProtocol = mock(PhyProtocol.class);
        PhyConfiguration mockPhyConfig = mock(PhyConfiguration.class);

        CPProtocol cpProtocol = mock(CPProtocol.class);

        CPCommandMessage malformedPrintCommand = new CPCommandMessage();
        malformedPrintCommand.create("invalidOrExpiredCookie", "print", "Hello, Server!", 2);

        // Mock the behavior getPhyProtocol() to return the mockPhyProtocol
        when(cpProtocol.getPhyProtocol()).thenReturn(mockPhyProtocol);

        CPServer.printMessage("invalidOrExpiredCookie", 2, "Hello, Server!");

        verify(mockPhyProtocol, never()).send(eq(malformedPrintCommand.getData()), eq(mockPhyConfig));
    }
}